<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        // $all_a =[
        // [
        //     'nama' => 'Profit-Loss Calculator',
        //     'gambar' => '/img/profitloss.jpg',
        //     'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero asperiores quaerat atque et dicta, tenetur ullam quae similique, voluptates minima iste ratione! Deleniti quasi numquam veritatis ea beatae, officiis odit.'
        // ],
        // [
        //     'nama' => 'Margin Calculator',
        //     'gambar' => '/img/margin.jpg',
        //     'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero asperiores quaerat atque et dicta, tenetur ullam quae similique, voluptates minima iste ratione! Deleniti quasi numquam veritatis ea beatae, officiis odit.'
        // ],
        // [
        //     'nama' => 'Break-Even Calculator',
        //     'gambar' => '/img/breakeven.jpg',
        //     'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero asperiores quaerat atque et dicta, tenetur ullam quae similique, voluptates minima iste ratione! Deleniti quasi numquam veritatis ea beatae, officiis odit.'
        // ],
        // [
        //     'nama' => 'Pricing Calculator',
        //     'gambar' => '/img/Pricing.jpg',
        //     'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero asperiores quaerat atque et dicta, tenetur ullam quae similique, voluptates minima iste ratione! Deleniti quasi numquam veritatis ea beatae, officiis odit.'
        // ]
        // ];

        $db = db_connect();
        $result = $db->query('SELECT * FROM gambar ORDER BY nama');
        $all_a = $result->getResult();

       // dd($all_a);

        return view('homepage', ['all_a' => $all_a]);
    }

    function hello(){
        echo "<h1>Hello........</h1>";
    }

    function welcome(){
        echo "<h1>Welcome</h1>";
    }

}
